variable "region" {
  type        = string
  default     = "LON1"
  description = "The name of the Civo region where the cluster nodes are to be provisioned"
}

variable "name" {
  type        = string
  default     = "gitlab-terraform-civo"
  description = "The name of the cluster to appear on the Civo Cloud Console"
}

variable "cluster_description" {
  type        = string
  default     = "This cluster is managed by GitLab"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}

variable "target_nodes_size" {
  type        = string
  default     = "g4s.kube.medium"
  description = "The size of the nodes to use for the cluster"
}

variable "num_target_nodes" {
  default     = 3
  description = "The number of cluster nodes"
}


variable "agent_version" {
  default     = "v14.8.1"
  description = "Agent version"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  description = "Agent token (provided when registering an Agent in GitLab)"
  sensitive   = true
}

variable "kas_address" {
  description = "Agent Server address (provided when registering an Agent in GitLab)"
}
