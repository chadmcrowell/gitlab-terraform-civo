terraform {
  required_providers {
    civo = {
      source  = "civo/civo"
      version = ">= 1.0.41"
    }
    helm = {
      source   = "hashicorp/helm"
      version  = ">= 2.13.2"
    }
  }
}


