# Infrastructure as Code with GitLab and Terraform (Civo Kubernetes)

This repository contains sample code for creating a Civo Kubernetes cluster using [GitLab Infrastructure as Code](https://docs.gitlab.com/ee/user/infrastructure/), and connecting it to GitLab with the [GitLab agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/).

For more information on how to use it, please refer to the [official docs]().
